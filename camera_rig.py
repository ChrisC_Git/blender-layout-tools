##########################################################
# This is a Simple Script to Build a Quick Camera Rig
# for Blender 3.3. Its bare bones with simple scripting to
# help beginners learn layout & scripting.
# When exporting right click, select hierachy, export to alembic
# TODO:
# - Clean up repetetive code
# ChrisC - 2022
##########################################################

import bpy, math
from mathutils import Vector
D = bpy.data
ops = bpy.ops
from .create_ctl_curves import (create_dolly_track_ctl, arrow_ctl, pan_tilt_roll_ctl)

################## Create Operator for New Camera ##################
class OBJECT_Create_CameraRig(bpy.types.Operator):
    ''' Creates a Camera with Control Rig'''
    bl_idname = "object.create_camerarig"
    bl_label = "Layout Tools - Create Camera Rig"
    bl_options = {'REGISTER', 'UNDO'} # adds the operator to Blenders Undo Stack

    # create properties
    cam_name: bpy.props.StringProperty(
        name='Camera Name:',
        default='Cam_',
        description='A unique name for this camera.'
    )
    lens_mm: bpy.props.IntProperty(
        name='Lens mm:',
        default=35,
        min=1, soft_max=200,
    )

    # make the operator only run in the 3D Viewport
    @classmethod
    def poll(cls, context):
        return context.area.type == 'VIEW_3D' # either true or false depending on the context

    def execute(self, context):
        result = create_camera(self.cam_name, self.lens_mm)
        if result == 'Duplicate Camera':
            self.report({'ERROR'}, 'A camera with the same name already exists!')
            return {'CANCELLED'}
        return {'FINISHED'}

    # Opens the window as a popup to force user to name camera
    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self)

# Only needed if you want to add into a dynamic menu.
def menu_func(self, context):
    self.layout.operator(OBJECT_Create_CameraRig.bl_idname, text="Dialog Operator")

# Register and add to the object menu (required to also use F3 search "Dialog Operator" for quick access)
def register():
    bpy.utils.register_class(OBJECT_Create_CameraRig)
    bpy.types.VIEW3D_MT_object.append(menu_func)

def unregister():
    bpy.utils.unregister_class(OBJECT_Create_CameraRig)

if __name__ == '__main__':
    register()

# Test call! So Uncomment this if you want it to run straight away
# bpy.ops.object.create_camerarig('INVOKE_DEFAULT')


################## Create Camera on OK ##################
def create_camera(cam_name, lens_mm):
    
    CameraName = cam_name+'_'
    
    scn = bpy.context.scene.objects
    for obj in scn:
        if obj.name == cam_name+'_renderCamera':
            print('Camera with this name already exists')
            return 'Duplicate Camera'

    # Zero 3D Cursor
    def zeroCursor():
        bpy.context.scene.cursor.location = Vector((0, 0, 0))
        bpy.context.scene.cursor.rotation_euler = Vector((0, 0, 0))

    # Create useful Functions
    def lockAttribs(object, *argv):
        '''
        here we are going to lock all attribs except the ones supplied to the function
        as *argv since most of the time we need them locked
        '''
        obj = bpy.data.objects[CameraName+object]

        # apply transforms to most controllers, first select the controller, then apply transform
        if obj.name != CameraName+'renderCamera' and obj.name != CameraName+'focus_ctl':
            bpy.data.objects[obj.name].select_set(True)
            ops.object.transform_apply(rotation=True, location=True, scale=True, properties=True)

        controls = ['tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz']
    
        # difference of the two lists, basically just the controls we want to lock
        new_list = []
        for control in controls:
            if control not in argv:
                new_list.append(control)
        
        for control in new_list:
            if control == 'tx':
                bpy.data.objects[obj.name].lock_location[0] = True
            if control == 'ty':
                bpy.data.objects[obj.name].lock_location[1] = True
            if control == 'tz':
                bpy.data.objects[obj.name].lock_location[2] = True
            if control == 'rx':
                bpy.data.objects[obj.name].lock_rotation[0] = True
            if control == 'ry':
                bpy.data.objects[obj.name].lock_rotation[1] = True
            if control == 'rz':
                bpy.data.objects[obj.name].lock_rotation[2] = True
            if control == 'sx':
                bpy.data.objects[obj.name].lock_scale[0] = True
            if control == 'sy':
                bpy.data.objects[obj.name].lock_scale[1] = True
            if control == 'sz':
                bpy.data.objects[obj.name].lock_scale[2] = True

    # START BUILDING RIG HERE

    # Zero Cursor
    zeroCursor()

    # Create Cameras Collection
    active_collection = bpy.context.view_layer.active_layer_collection.collection
    
    collection_name = "Cameras"
    collection_found = False

    for collection in bpy.data.collections:
        if collection.name == collection_name:
            collection_found = True
            # set active collection so camera is made here
            layer_collection = bpy.context.view_layer.layer_collection.children[collection.name]
            bpy.context.view_layer.active_layer_collection = layer_collection
            break
    
    if not collection_found:
        collection = bpy.data.collections.new(collection_name)
        collection.color_tag = "COLOR_01"
        bpy.context.scene.collection.children.link(collection)
        # set active collection so camera is made here
        layer_collection = bpy.context.view_layer.layer_collection.children[collection.name]
        bpy.context.view_layer.active_layer_collection = layer_collection

    # Create Camera
    renderCamera = ops.object.camera_add(rotation=(1.5708,0,0))
    bpy.context.selected_objects[0].name = CameraName+'renderCamera'
    bpy.context.selected_objects[0].data.lens = lens_mm
    bpy.context.selected_objects[0].data.clip_start = 0.01
    bpy.context.selected_objects[0].data.sensor_fit = 'HORIZONTAL'
    bpy.context.selected_objects[0].data.sensor_width = 36 # change this to match your film back size so that Focal Lengths match. Maya 2.35 aspect requires 56.4mm
    bpy.context.selected_objects[0].data.passepartout_alpha = 1
    bpy.context.selected_objects[0].data.show_name = True
    bpy.context.selected_objects[0].data.dof.use_dof = True
    bpy.context.selected_objects[0]["LayoutRig"] = 'True'

    # Create Controllers and Position
    '''
    Rotation is in radians not degrees, thus can use 'radian = degree x pi/2' to convert values.
    Can also use math.radians(dregee)
    https://blender.stackexchange.com/questions/167830/how-to-rotate-an-object
    https://devtalk.blender.org/t/bpy-ops-transform-rotate-option-axis/6235/4
    '''
    focus_ctl = ops.curve.primitive_bezier_circle_add(radius=0.1, rotation=(1.5708,0,0), location=(0,2,0))
    bpy.context.selected_objects[0].name = CameraName+'focus_ctl'

    roll_ctl = pan_tilt_roll_ctl(f'{CameraName}roll_ctl')
    bpy.context.object.rotation_euler = Vector((1.5708, 0, 0))
    bpy.context.object.scale = Vector((0.9, 0.9, 0.9))
    bpy.context.object.location = Vector((0, 0, 0.75))

    tilt_ctl = pan_tilt_roll_ctl(f'{CameraName}tilt_ctl')
    bpy.context.object.rotation_euler = Vector((1.5708, 0, 1.5708))
    bpy.context.object.scale = Vector((0.9, 0.9, 0.9))
    bpy.context.object.location = Vector((0, 0, 0.75))

    pan_ctl = pan_tilt_roll_ctl(f'{CameraName}pan_ctl')
    bpy.context.object.rotation_euler = Vector((0, 0, 3.1416))
    bpy.context.object.scale = Vector((0.9, 0.9, 0.9))
    bpy.context.object.location = Vector((0, -0.9, 0))

    crane_ctl = arrow_ctl(f'{CameraName}crane_ctl')
    bpy.context.object.rotation_euler = Vector((1.5708, 0, 1.5708))
    bpy.context.object.scale = Vector((0.5, 0.5, 0.5))
    bpy.context.object.location = Vector((0, 0, 0.55))

    dolly_track_ctl = create_dolly_track_ctl(f'{CameraName}dolly_track_ctl')
    bpy.context.object.scale = Vector((0.9, 0.9, 0.9))

    COG_ctl = ops.curve.primitive_bezier_circle_add(radius=1, rotation=(0,0,0))
    bpy.context.selected_objects[0].name = CameraName+'COG_ctl'

    local_ctl = ops.curve.primitive_bezier_circle_add(radius=0.01, rotation=(0,0,0))
    bpy.context.selected_objects[0].name = CameraName+'local_ctl'

    # Some curve clean up. Removing the Path Animation attrib (messes with Parenting)
    scn = bpy.context.scene.objects
    for obj in scn:
        if obj.type == 'CURVE':
            bpy.data.objects[obj.name].data.use_path = False
            
    # Setup focus_ctl
    bpy.data.objects[CameraName+'renderCamera'].data.dof.focus_object = bpy.data.objects[CameraName+'focus_ctl']

    # Lock attribs, list attribs you want to keep unlocked
    lockAttribs('renderCamera')
    lockAttribs('focus_ctl', 'ty')
    lockAttribs('roll_ctl', 'ry')
    lockAttribs('tilt_ctl', 'rx')
    lockAttribs('pan_ctl', 'rz')
    lockAttribs('crane_ctl', 'tz')
    lockAttribs('dolly_track_ctl', 'tx', 'ty')

    # Parent Heirachy
    bpy.data.objects[CameraName+'focus_ctl'].parent = bpy.data.objects[CameraName+'roll_ctl']
    bpy.data.objects[CameraName+'renderCamera'].parent = bpy.data.objects[CameraName+'roll_ctl']
    bpy.data.objects[CameraName+'roll_ctl'].parent = bpy.data.objects[CameraName+'tilt_ctl']
    bpy.data.objects[CameraName+'tilt_ctl'].parent = bpy.data.objects[CameraName+'pan_ctl']
    bpy.data.objects[CameraName+'pan_ctl'].parent = bpy.data.objects[CameraName+'crane_ctl']
    bpy.data.objects[CameraName+'crane_ctl'].parent = bpy.data.objects[CameraName+'dolly_track_ctl']
    bpy.data.objects[CameraName+'dolly_track_ctl'].parent = bpy.data.objects[CameraName+'COG_ctl']
    bpy.data.objects[CameraName+'COG_ctl'].parent = bpy.data.objects[CameraName+'local_ctl']
    
    # Restore old active collection
    try:
        layer_collection = bpy.context.view_layer.layer_collection.children[active_collection.name]
        bpy.context.view_layer.active_layer_collection = layer_collection
    except KeyError:
        pass
        # I need to find out how to set children collections still :p
    
    # deselect all
    bpy.ops.object.select_all(action='DESELECT')