import bpy

def create_ctl(name, verts):
    '''
    Create Camera Controller
    Technique: https://blender.stackexchange.com/questions/61879/create-mesh-then-add-vertices-to-it-in-python
    '''
    mesh = bpy.data.meshes.new(name)  # add the new mesh
    obj = bpy.data.objects.new(mesh.name, mesh)
    
    # get active collection
    col = bpy.context.view_layer.active_layer_collection.collection
    col.objects.link(obj)
    bpy.context.view_layer.objects.active = obj

    edges = []
    for vert in range(len(verts)):
        value = vert, (vert+1) % len(verts)
        edges.append(value)
    
    faces = [] # do not want to draw faces
    mesh.from_pydata(verts, edges, faces)

def create_dolly_track_ctl(name):
    ''' Shape for Dolly Track Control '''
    verts = [(0.0, -1.0, 0.0),
            (-0.4, -0.6, 0.0),
            (-0.2, -0.6, 0.0),
            (-0.2, -0.2, 0.0),
            (-0.6, -0.2, 0.0),
            (-0.6, -0.4, 0.0),
            (-1.0, -0.0, 0.0),
            (-0.6, 0.4, 0.0),
            (-0.6, 0.2, 0.0),
            (-0.2, 0.2, 0.0),
            (-0.2, 0.6, 0.0),
            (-0.4, 0.6, 0.0),
            (-0.0, 1.0, 0.0),
            (0.4, 0.6, 0.0),
            (0.2, 0.6, 0.0),
            (0.2, 0.2, 0.0),
            (0.6, 0.2, 0.0),
            (0.6, 0.4, 0.0),
            (1.0, 0.0, 0.0),
            (0.6, -0.4, 0.0),
            (0.6, -0.2, 0.0),
            (0.2, -0.2, 0.0),
            (0.2, -0.6, 0.0),
            (0.4, -0.6, 0.0),
            (0.0, -1.0, 0.0),
            ]
    create_ctl(name, verts)

def arrow_ctl(name):
    ''' Shape for Arrow Control '''
    verts = [(-0.3, -1.0, 0.0),
            (-0.3, 0.2, 0.0),
            (-0.7, 0.2, 0.0),
            (-0.0, 1.0, 0.0),
            (0.7, 0.2, 0.0),
            (0.3, 0.2, 0.0),
            (0.3, -1.0, 0.0),
            (-0.3, -1.0, 0.0),
            ]
    create_ctl(name, verts)
    
def pan_tilt_roll_ctl(name):
    ''' Shape for Arrow Control '''
    verts = [(-0.806, -0.025, 0.0),
            (-0.883, 0.469, 0.0),
            (-0.770, 0.409, 0.0),
            (-0.583, 0.638, 0.0),
            (-0.390, 0.777, 0.0),
            (-0.196, 0.844, 0.0),
            (0.0, 0.872, 0.0),
            (0.196, 0.844, 0.0),
            (0.390, 0.777, 0.0),
            (0.583, 0.638, 0.0),
            (0.770, 0.409, 0.0),
            (0.883, 0.469, 0.0),
            (0.806, -0.025, 0.0),
            (0.352, 0.190, 0.0),
            (0.481, 0.259, 0.0),
            (0.387, 0.385, 0.0),
            (0.210, 0.5, 0.0),
            (0.0, 0.546, 0.0),
            (-0.210, 0.5, 0.0),
            (-0.387, 0.385, 0.0),
            (-0.481, 0.259, 0.0),
            (-0.352, 0.190, 0.0),
            (-0.806, -0.025, 0.0),
            ]
    create_ctl(name, verts)