# Blender Layout Tools

## About

Created largely for my own sanity, but also after seeing so many Blender users just using a raw camera and keying directly on the camera itself. This repo will hopefully grow to be a full set of tools for Layout Artists using Blender.

A set of very simple scripts to help a Layout Artist perform thier day to day task of Creating Cameras for Director Review.

The toolset currently includes two tools:
- A camera rig.
- An offset camera animation operator.

## Camera Rig
A camera contol rig that is designe to help Layout Artists create beautiful cinematic camera shots for thier films.

Hitting F3 and typing 'Create Camera Rig' (or part of...) will allow you to drop down and create a new camera with a control rig.

![Camera Rig](https://www.cunnington.co.za/blog/images/blender_simple_camera_rig.png)

## Offset Camera Animation
A tool for Layout Artists to quickly offset thier sequence animation to a single start frame for Animators to work in Shot Context.

Hitting F3 and typing 'Offset Animation on All Camera's' (or part of...) will invoke a tool that looks to the timeline for markers where different camera at marked to change. It will then ask you for the new start frame such as 1, and then slide all the animation on those cameras up to start at frame 1.

## To Do:
### Create Camera Rig
- [ ] If duplicate camera name detected end script, and throw error. Currently the camera gets renamed automatically
- [ ] Add code to restore previous active collection if it was a nested child collection
- [ ] Clean up some of the repetivite code
- [ ] Add colour to curves, when blender adds this feature: https://developer.blender.org/T102605