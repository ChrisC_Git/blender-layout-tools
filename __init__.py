# ----------------------------------------------------------
# Author: Chris Cunningotn (flipsideza)
# ----------------------------------------------------------

# ----------------------------------------------
# Define Addon info
# ----------------------------------------------

bl_info = {
    'name': 'Blender Layout Tools',
    'author': 'Chris C <www.cunnington.co.za>',
    'version': (0, 0, 2),
    'blender': (3, 3, 1),
    'category': '3D View',
    'location': 'Operator - Hit F3 then Type `Layout`',
    "doc_url": "www.cunnington.co.za/blog",
    'description': 'Tools for Layout Artists.',
}

# ----------------------------------------------
# Import modules
# ----------------------------------------------

import bpy
import os

from . import camera_rig
from . import offset_camera_anim
from . import create_ctl_curves

# --------------------------------------------------------------
# Registration
# --------------------------------------------------------------

def register():
    camera_rig.register()
    offset_camera_anim.register()

def unregister():
    camera_rig.unregister()
    offset_camera_anim.unregister()

if __name__ == "__main__":
    register()
