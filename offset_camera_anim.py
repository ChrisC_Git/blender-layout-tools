##########################################################
# This is a Simple Script that moves the keys on the 
# timeline from the camera'a marker position to frame 1. 
# Its bare bones with simple scripting to help beginners 
# learn layout & scripting.
# TODO:
# ChrisC - 2022
##########################################################
import bpy

################## Create Operator ##################
class CMD_Offset_CameraAnimation(bpy.types.Operator):
    '''Offsets the Animation on All Camera's based on Markers'''
    bl_idname = "object.offset_camera_animation"
    bl_label = "Layout Tools - Shot Camera Breakout Tool"

    start_frame: bpy.props.IntProperty(
        name="Start Frame: ",
        default=1,
        min=1, soft_max=101,
        )
        
    # make the operator only run in the 3D Viewport
    @classmethod
    def poll(cls, context):
        return context.area.type == 'VIEW_3D' # either true or false depending on the context

    def execute(self, context):
        file_saved = save_backup_file()
        if file_saved == 'Scene Not Saved':
            self.report({'ERROR'}, 'Please Save Your Scene')
            return {'CANCELLED'}
        result = offset_camera_animation(self.start_frame)
        if result == 'No Markers':
            self.report({'ERROR'}, 'No Markers Found')
            return {'CANCELLED'}
        return {'FINISHED'}

    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self)

# Only needed if you want to add into a dynamic menu.
def menu_func(self, context):
    self.layout.operator(CMD_Offset_CameraAnimation.bl_idname, text="Dialog Operator")

# Register and add to the object menu (required to also use F3 search "Dialog Operator" for quick access)
def register():
    bpy.utils.register_class(CMD_Offset_CameraAnimation)
    #bpy.types.VIEW3D_MT_object.append(menu_func)

def unregister():
    bpy.utils.unregister_class(CMD_Offset_CameraAnimation)

if __name__ == '__main__':
    register()

# Test call! So Uncomment this if you want it to run straight away
# bpy.ops.object.create_camerarig('INVOKE_DEFAULT')

################## Script to Run ##################

def move_keyframes(animation_action, offset_value):
    ''' Here we move keys on the timeline. We supply the function with a:
    https://maglit.me/moveallkeyframes
    '''
    for fcurve in animation_action.fcurves:
        for point in fcurve.keyframe_points:
            key_frame = point.co.x
            key_value = point.co.y
            # print (key_frame, key_value)
            
            # move the keys
            point.co.x -= offset_value
            # don't forget the keyframe's handles:
            point.handle_left.x -= offset_value
            point.handle_right.x -= offset_value

def getParents(myObject): 
    parents = [myObject] # remove myObject if you dont want to include selected
    while myObject.parent:
        myObject = myObject.parent
        parents.append(myObject)
        parents.reverse()
    return parents

def save_backup_file():
    if bpy.data.filepath == '':
        print('Scene Not Saved')
        return 'Scene Not Saved'
    else:
        print('Saving Backup....')
        file_path = bpy.data.filepath
        bpy.ops.wm.save_mainfile(filepath=file_path.replace('.blend', '_breakout.blend'))

def offset_camera_animation(start_frame):
    '''
    Here we calculate the start frame offset and then
    move the camera animation as needed.
    https://maglit.me/framewithmarker
    '''
    # Check if we have any markers
    if bpy.context.scene.timeline_markers.items() == []:
        return 'No Markers'
    # Get the camera bind frame number and camera name
    for marker in bpy.context.scene.timeline_markers:
        obj = bpy.context.scene.objects[marker.camera.name]
        bpy.context.view_layer.objects.active = obj   # Make the cube the active object 
        bpy.ops.object.select_all(action='DESELECT') # Deselect all objects
        obj.select_set(True) # select object
        
        offset_frame = marker.frame-start_frame
        
        # select all the controllers
        cam_controls = getParents(marker.camera)
        
        for control in cam_controls:
            if control is not None:
                run = None
                if control.data.animation_data is not None and control.data.animation_data.action is not None:
                    # for CAMERAS - bpy.context.scene.objects['XXXX'].data.animation_data.action
                    action = control.data.animation_data.action
                    run = True
                elif control.animation_data is not None and control.animation_data.action is not None:
                    # for MESH    - bpy.context.scene.objects['XXXX'].animation_data.action
                    action = control.animation_data.action
                    run = True
                
                if run:
                    #print(f'running time change on {control.name}')
                    move_keyframes(action, offset_frame)

    # remove markers from scene to reduce confusion
    bpy.context.scene.timeline_markers.clear()